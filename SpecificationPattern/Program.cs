﻿using System;
using System.Collections.Generic;
using System.Linq;
using SpecificationPattern.ProductSpecification;
using SpecificationPattern.Specification;

namespace SpecificationPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            var testData = new TestData();

            var list = testData.Generate();
            Console.WriteLine("List all products");
            Print(list);

            var specMaxPrice = new MaximumPriceProductSpecification(5);
            var filteredByMaxPrice = list.Where(specMaxPrice.IsSatisfiedBy);
            Console.WriteLine();
            Console.WriteLine("Maximum price: 5" );
            Print(filteredByMaxPrice);

            var specContainsLetter = new TitleContainsCharacter('2');
            var filteredByContainingLetter = list.Where(specContainsLetter.IsSatisfiedBy);
            Console.WriteLine();
            Console.WriteLine("Title contains character '2' ");
            Print(filteredByContainingLetter);


            var spec = new HasDescriptionProductSpecification()
                    .And(new MinimumPriceProductSpecification(8));

            var filtered = list.Where(spec.IsSatisfiedBy);
            Console.WriteLine();
            Console.WriteLine("has description and minimal price 8");
            Print(filtered);

            Console.ReadKey();
        }

        private static void Print(IEnumerable<Product> products)
        {
            foreach (var product in products)
            {
                Print(product);
            }
        }

        private static void Print(Product product)
        {
            Console.WriteLine($"Title: {product.Title} | Price:{product.Price} | Description:{product.Description}");
        }

    }

}
