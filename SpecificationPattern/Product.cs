﻿using System;

namespace SpecificationPattern
{
    public class Product
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public decimal Price { get; set; }
        public string UnitOfMeasure { get; set; }
    }
}
