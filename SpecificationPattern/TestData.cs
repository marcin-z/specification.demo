﻿using System;
using System.Collections.Generic;

namespace SpecificationPattern
{
    public class TestData
    {
        public List<Product> Generate()
        {
            var list = new List<Product>();
            Random rnd = new Random();

            for (int i = 1; i <= 10; i++)
            {
                list.Add(new Product()
                {
                    Id = i,
                    Created = DateTime.Now.AddDays(rnd.Next(0, 5)),
                    UnitOfMeasure = "P",
                    Description = $"Product {i} description",
                    Title = $"Title of Product {i}",
                    Price = Convert.ToDecimal(i)
                });
            }

            return list;
        }
    }

    static class RandomExtensions
    {
        public static int NextInt32(this Random rng)
        {
            unchecked
            {
                int firstBits = rng.Next(0, 1 << 4) << 28;
                int lastBits = rng.Next(0, 1 << 28);
                return firstBits | lastBits;
            }
        }

        public static decimal NextDecimal(this Random rng)
        {
            byte scale = (byte)rng.Next(29);
            bool sign = rng.Next(2) == 1;
            return new decimal(rng.NextInt32(),
                rng.NextInt32(),
                rng.NextInt32(),
                sign,
                scale);
        }
    }

}
