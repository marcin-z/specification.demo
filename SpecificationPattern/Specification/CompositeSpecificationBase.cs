﻿namespace SpecificationPattern.Specification
{
    public abstract class CompositeSpecificationBase<T> : ISpecification<T>
    {
        protected CompositeSpecificationBase(ISpecification<T> leftSpec, ISpecification<T> rightSpec)
        {
            LeftSpec = leftSpec;
            RightSpec = rightSpec;
        }

        public ISpecification<T> LeftSpec { get; }

        public ISpecification<T> RightSpec { get; }

        public abstract bool IsSatisfiedBy(T param);

    }
}
