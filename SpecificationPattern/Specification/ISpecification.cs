﻿namespace SpecificationPattern.Specification
{
    public interface ISpecification<in T>
    {
        bool IsSatisfiedBy(T param);
    }
}
