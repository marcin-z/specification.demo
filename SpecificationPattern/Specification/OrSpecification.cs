﻿namespace SpecificationPattern.Specification
{
    public class OrSpecification<T> : CompositeSpecificationBase<T>
    {
        public OrSpecification(ISpecification<T> leftSpec, ISpecification<T> rightSpec) 
            : base(leftSpec, rightSpec)
        {}

        public override bool IsSatisfiedBy(T param)
        {
            return LeftSpec.IsSatisfiedBy(param) || RightSpec.IsSatisfiedBy(param);
        }
    }
}
