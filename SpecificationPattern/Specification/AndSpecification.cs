﻿namespace SpecificationPattern.Specification
{
    public class AndSpecification<T> : CompositeSpecificationBase<T>
    {
        public AndSpecification(ISpecification<T> leftSpec, ISpecification<T> rightSpec) 
            : base(leftSpec, rightSpec)
        {}

        public override bool IsSatisfiedBy(T param)
        {
            return LeftSpec.IsSatisfiedBy(param) && RightSpec.IsSatisfiedBy(param);
        }
    }
}
