﻿using SpecificationPattern.Specification;

namespace SpecificationPattern.ProductSpecification
{
    public class MaximumPriceProductSpecification : ISpecification<Product>
    {
        private readonly decimal _price;

        public MaximumPriceProductSpecification(decimal price)
        {
            this._price = price;
        }

        public bool IsSatisfiedBy(Product param)
        {
            return param.Price <= _price;
        }
    }
}
