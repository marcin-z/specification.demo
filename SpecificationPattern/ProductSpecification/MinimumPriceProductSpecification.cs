﻿using SpecificationPattern.Specification;

namespace SpecificationPattern.ProductSpecification
{
    public class MinimumPriceProductSpecification : ISpecification<Product>
    {
        private readonly decimal _minPrice;
        public MinimumPriceProductSpecification(decimal minPrice)
        {
            _minPrice = minPrice;
        }

        public bool IsSatisfiedBy(Product param)
        {
            return param.Price >= _minPrice;
        }
    }
}
