﻿using SpecificationPattern.Specification;

namespace SpecificationPattern.ProductSpecification
{
    public class HasDescriptionProductSpecification : ISpecification<Product>
    {
        public bool IsSatisfiedBy(Product param)
        {
            return !string.IsNullOrWhiteSpace(param.Description);
        }
    }
}
