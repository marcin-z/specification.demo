﻿using SpecificationPattern.Specification;
using System.Linq;

namespace SpecificationPattern.ProductSpecification
{
    public class TitleContainsCharacter : ISpecification<Product>
    {
        private readonly char _c;
        public TitleContainsCharacter(char c)
        {
            _c = c;
        }
        public bool IsSatisfiedBy(Product product)
        {
            return product.Title.Contains(_c);
        }
    }
}
